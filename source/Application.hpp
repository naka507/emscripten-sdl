#ifndef Application_HPP
#define Application_HPP

#include "Window.hpp"

class Application : public Window
{
public:

    Application();
    ~Application();
  
protected:
    virtual void render(float dt);
    virtual void input(float dt);
};

#endif