#include "Window.hpp"

#include <functional>
#include <iostream>
#include <stdexcept>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/html5.h>
#include <cmath>
#endif
#include <SDL_image.h>
#include <SDL_ttf.h>

std::function<void()> registered_loop;
void main_loop()
{
    registered_loop();
}

Window *currentWindow = NULL;

Window &Window::getInstance()
{
    if (currentWindow)
        return *currentWindow;
    else
        throw std::runtime_error("There is no current Window");
}

Window::Window()
    : state(stateReady), width(SCREEN_WIDTH), height(SCREEN_HEIGHT)
{
    currentWindow = this;

    if (SDL_Init(SDL_INIT_VIDEO))
    {
        std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
        throw std::runtime_error("Couldn't init SDL");
    }

    if (!IMG_Init(IMG_INIT_PNG))
    {
        std::cout << "IMG_Init Error: " << IMG_GetError() << std::endl;
        throw std::runtime_error("Couldn't init SDL");
    }

    // if (TTF_Init() == -1)
    // {
    //     std::cout << "TTF_Init Error: " << TTF_GetError() << std::endl;
    //     throw std::runtime_error("Couldn't init SDL");
    // }

    window = SDL_CreateWindow("Emscripten SDL2 Demo", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_RESIZABLE);

    if (window == nullptr)
    {
        std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
        throw std::runtime_error("Couldn't init SDL");
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr)
    {
        SDL_DestroyWindow(window);
        std::cout << "SDL_CreateRender Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        throw std::runtime_error("Couldn't init SDL");
    }
}

SDL_Window *Window::getWindow() const
{
    return window;
}

SDL_Renderer *Window::getRenderer() const
{
    return renderer;
}

int Window::getWidth()
{
    return width;
}

int Window::getHeight()
{
    return height;
}

void Window::exit()
{
    state = stateExit;
}

void Window::run()
{
    state = stateRun;

    float time = 0.0f;
    float frame = 0.0f;

    registered_loop = [&]()
    {
        float current = static_cast<float>(SDL_GetTicks());
        time = current - frame;
        frame = current;

        SDL_SetRenderDrawColor(getRenderer(), 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(getRenderer());

        change();

        render(time);

        SDL_RenderPresent(getRenderer());
    };

#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(main_loop, 0, 1);
#else

    do
    {
        SDL_Event event;
        SDL_WaitEvent(&event);
        switch (event.type)
        {
        case SDL_QUIT:
            SDL_Log("Event type is %d", event.type);
            state = stateExit;
        case SDL_KEYDOWN:
            SDL_Log("Event type is %d", event.type);
        default:
            SDL_Log("Event type is %d", event.type);
            break;
        }

        main_loop();

    } while (state == stateRun);

    SDL_DestroyRenderer(getRenderer());
    SDL_DestroyWindow(getWindow());
    SDL_Quit();
#endif
}

void Window::change()
{
    int w, h;
#ifdef __EMSCRIPTEN__
    emscripten_get_canvas_element_size("canvas", &w, &h);
#else
    SDL_GetWindowSize(getWindow(), &w, &h);
#endif
    changed = (w != width || h != height);
    width = w;
    height = h;
    SDL_SetWindowSize(getWindow(), width, height);
    SDL_RenderSetLogicalSize(getRenderer(), width, height);
}