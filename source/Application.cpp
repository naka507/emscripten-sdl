#include "Application.hpp"
#include "Texture.hpp"
#include "SDL_image.h"

#include <iostream>
#include <Asset.hpp>

Texture gTextTexture;

Application::Application()
{

    if (!gTextTexture.loadFromFile(this->getRenderer(), RESOURCES_DIR "/texture/colors.png"))
    {
        std::cout << "Failed to render text texture!" << std::endl;
    }

    // TTF_Font *font = TTF_OpenFont(RESOURCES_DIR "/fonts/Antonio-Bold.ttf", 32);

    // SDL_Color color = {0, 0, 0};
    // if (!gTextTexture.loadFromRenderedText(this->getRenderer(), font, "The quick brown fox jumps over the lazy dog", color))
    // {
    //     std::cout << "Failed to render text texture!" << std::endl;
    // }

    std::cout << "Application Init OK! " << std::endl;
}

Application::~Application()
{
    gTextTexture.free();
}

void Application::input(float deltaTime)
{
}

void Application::render(float deltaTime)
{

    Uint8 r = 255;
    Uint8 g = 255;
    Uint8 b = 255;

    gTextTexture.setColor(r, g, b);
    gTextTexture.render(this->getRenderer(), (this->getWidth() - gTextTexture.getWidth()) / 2, (this->getHeight() - gTextTexture.getHeight()) / 2);


    // std::cout << "Application loop : " << this->getWidth() << "," << this->getHeight() << std::endl;
}
