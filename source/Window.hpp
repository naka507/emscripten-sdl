

#ifndef WINDOW_HPP
#define WINDOW_HPP
#include <SDL2/SDL.h>
#include <string>

const unsigned int SCREEN_WIDTH = 800;
const unsigned int SCREEN_HEIGHT = 600;

struct SDL_Window;
enum State
{
    stateReady,
    stateRun,
    stateExit
};
class Window
{
public:
    Window();

    static Window &getInstance();

    SDL_Window *getWindow() const;
    SDL_Renderer *getRenderer() const;

    int getWidth();
    int getHeight();

    void run();
    void exit();

private:
    State state;

    Window &operator=(const Window &) { return *this; }

    SDL_Window *window;
    SDL_Renderer *renderer;

    float deltaTime;

    int width;
    int height;
    bool changed;
    void change();

protected:
    Window(const Window &){};

    virtual void render(float dt) = 0;
    virtual void input(float dt) = 0;
};

#endif
